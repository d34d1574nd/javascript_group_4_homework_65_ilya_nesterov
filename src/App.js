import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import './App.css';

import HomePage from './containers/HomePage/HomePage';
import Page from "./components/Page/Page";
import Layout from "./components/Layout/Layout";
import Admin from "./containers/Admin/Admin";

class App extends Component {
    render() {
    return (
<Layout>
    <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/pages/admin" exact component={Admin} />
        <Route path="/pages/:name" exact component={Page} />
        <Route render={() => <h1>Not found</h1>} />
    </Switch>
</Layout>
    );
  }
}

export default App;
