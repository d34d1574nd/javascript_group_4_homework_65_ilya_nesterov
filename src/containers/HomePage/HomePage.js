import React, {Component} from 'react';
import axios from '../../axiosBase';

import './HomePage.css';

import Blog from '../../assets/blog.jpg';

class HomePage extends Component {

    componentDidMount() {
        axios.get('pages.json')
    }

    render() {
        return (
            <div className='containers'>
                <img src={Blog} alt='Blog' className='imageHome'/>
                <p className="homePageText"><strong>Привет</strong> всем. Вы на главной странице самого крутого блога во всем интернете. Здесь вы найдете все про туризм,
                    еду (приготовление) и много полезного. Так же скоро запущу страницу с любимыми книжками и хобби. Подписывайтесь на уведомления и следите
                    за моей жизнью!
                </p>
            </div>
        );
    }
}

export default HomePage;