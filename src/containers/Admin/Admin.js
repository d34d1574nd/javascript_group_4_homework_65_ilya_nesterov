import React, {Component} from 'react';
import axios from '../../axiosBase';

import './Admin.css';

class Admin extends Component {
    state = {
        categoryId: [],
        category: '',
        title: '',
        content: '',
        img: '',
    };

    async componentDidMount() {
        const response = await axios.get('pages.json');
        this.setState({categoryId: response.data})
    }

    changeCategory = event => {
        this.setState({category: event.target.value});
        axios.get('pages/' + event.target.value + '.json').then(response => {
            this.setState({title: response.data.title, content: response.data.content, img: response.data.img});
        })
    };
    changeTitle = event => {
        this.setState({title: event.target.value})
    };
    changeContent = event => {
        this.setState({content: event.target.value})
    };
    changeImg = event => {
        this.setState({img: event.target.value})
    };

    editPage = event => {
        event.preventDefault();

        const page = {
            title: this.state.title,
            content: this.state.content,
            img: this.state.img,
        };

        axios.put('pages/' + this.state.category + '.json', page).then(() => {
            this.props.history.push('/')
        });
    };

    render() {
        console.log(this.state.category);
        return (
            <div className='containers'>
                <h2>Edit page</h2>
                <form>
                    <label htmlFor="category">Category:</label>
                    <select name="category" id="category"  onChange={this.changeCategory} value={this.state.category}>
                        <option hidden>--- Select category ---</option>
                        {Object.keys(this.state.categoryId).map(categoryId => (
                            <option key={categoryId} value={categoryId}>{categoryId}</option>
                        ))}
                    </select>
                    <label htmlFor='title'>Title:</label>
                    <input type="text" id='title' value={this.state.title || ''} onChange={this.changeTitle}/>
                    <label htmlFor='image'>Ссылка на картинку:</label>
                    <input type="text" id='image' value={this.state.img || ''} onChange={this.changeImg}/>
                    <label htmlFor='content'>Контент:</label>
                    <textarea typeof='text' id='content'  value={this.state.content || ''} onChange={this.changeContent}/>
                    <button onClick={this.editPage}>Edit page</button>
                </form>
            </div>
        );
    }
}

export default Admin