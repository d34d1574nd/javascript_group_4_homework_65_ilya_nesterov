import React from 'react';
import NavigationItem from "./NavigatonItem/NavigationItem";

import './NavigationItems.css';

const NavigationItems = () => {
    return (
    <ul className="NavigationItems">
        <NavigationItem to="/" exact>Home</NavigationItem>
        <NavigationItem to="/pages/blog" exact>Blog</NavigationItem>
        <NavigationItem to="/pages/products" exact>Products</NavigationItem>
        <NavigationItem to="/pages/contacts" exact>Contacts</NavigationItem>
        <NavigationItem to="/pages/about" exact>About</NavigationItem>
        <NavigationItem to="/pages/admin" exact>Admin</NavigationItem>
    </ul>
)};

export default NavigationItems;