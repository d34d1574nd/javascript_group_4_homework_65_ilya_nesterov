import React from 'react';

import './Toolbar.css';
import Logo from '../../../assets/logo.png';

import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = () => (
        <header className="Toolbar">
            <img src={Logo} alt="Logo" className="Logo"/>
            <nav>
                <NavigationItems />
            </nav>
        </header>
    );

export default Toolbar;