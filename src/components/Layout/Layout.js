import React, {Fragment} from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";
import Footer from "../Footer/Footer";

import './Layout.css';

const Layout = ({children}) => (
    <Fragment>
        <Toolbar />
        <main className="layout">
            {children}
        </main>
        <Footer />
    </Fragment>
);

export default Layout;