import React, {Component} from 'react';

import './Page.css';

import axios from '../../axiosBase';
import Spinner from "../UI/Spinner/Spinner";

class Page extends Component {

    state = {
        title: '',
        content: '',
        img: '',
        loading: true,
    };

    loadData = () => {
        const idUrl = this.props.match.params.name;

            axios.get('pages/' + idUrl + '.json').then(response => {
                this.setState({title: response.data.title, content: response.data.content, img: response.data.img})
            }).finally(() => {
                this.setState({loading: false});
            })
    };

    componentDidMount() {
        this.loadData();
    };

    componentDidUpdate(prevProps) {
        if (this.props.match.params.name !== prevProps.match.params.name) {
            this.loadData()
        }
    }

    render() {
        if (this.state.loading) {
            return <Spinner />;
        }

        return (
            <div className='div'>
                <div className="image">
                    <img src={this.state.img} alt={this.state.img}/>
                </div>
                <div className="text">
                    <strong className='title'>{this.state.title}</strong>
                    <p className='content'>{this.state.content}</p>
                </div>
            </div>
        );
    }
}

export default Page;