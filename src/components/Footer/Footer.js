import React from 'react';

import './Footer.css';

const Footer = () => (
        <footer className='footer'>
            <p>Property by Ilya Nesterov</p>
        </footer>
    );

export default Footer;